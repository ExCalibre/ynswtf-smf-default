# SMF Theme Build
Bu tema 2019 teknoloji kullanılarak geliştirilmektedir. Genel olarak üretilicek temaların ana projesidir.
## Gereksinimler

composer
npm
gulp (global)
bower

## Kullanımı

		git clone https://gitlab.com/ynswtf/ynswtf-smf-theme smf-theme
		bower install
		npm install
		composer install
		npm run dev
		gulp default
		
Ardından proje geliştirilmeye açık olacaktır. Bu paket geliştirme paketidir yani development aşamasındadır. Eğer bu paketi projenizde canlıda kullanacaksanız. Releases sürümlerini indirmeniz gerekecektir.