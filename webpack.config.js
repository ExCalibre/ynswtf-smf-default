const VueLoaderPlugin = require('vue-loader/lib/plugin');
const path = require('path');
module.exports = [
    {
        entry: {
            "index": './build/js/index.js',
        },
        output: {
            filename: "[name].js",
            path: path.resolve(__dirname, 'app/bundle')
        },
        module: {
            rules: [
                { 
                    test: /\.vue$/, 
                    loader: "vue-loader" 
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.css$/,
                    use: [
                        'vue-style-loader',
                        'css-loader'
                    ]
                }
            ]
        },
        plugins: [
            new VueLoaderPlugin()
        ]
    }
];