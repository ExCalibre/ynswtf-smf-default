<?php 
require_once $settings['theme_dir'] . '/app/vendor/autoload.php';

use Wibuzz\Application as App;
use Wibuzz\Helper\Render;

$application = new App($settings);
$render = new Render();
$app = [
    'path' => $application->path
];


?>