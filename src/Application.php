<?php
namespace Wibuzz;

class Application{

    public $path;

    public $url;

    public function __construct( $settings ){
        $this->path = $settings['theme_dir'] . '\\';
        $this->url = $settings['theme_dir'] . '\\';
    }

    public static function path(){
        return $this->path;
    }
}
?>