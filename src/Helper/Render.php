<?php 
namespace Wibuzz\Helper;

use Wibuzz\Application as App;

class Render{

    public $require;

    public $path;

    public $values;


    public function __construct(){
        $this->path = App::path();
        $this->require = $this->path.'/views/blank.php';
    }

    public function view( $render = null , $value = [] ){
        if(!$render){
            return false;
        }

        if(!file_exists($this->path.'/'.$render)){
            echo '<div class="uk-alert-danger" uk-alert>Bu Dosya Yok: '.$render.'</div>';
            return false;
        }

        if(is_array($value) || $value){
            $this->values = $value;
        }

        $this->require = $this->path.'/'.$render;
        return;
    }

    public function renderRequire(){
        if(is_array($this->values) || !$this->values){
            extract($this->values);
        }
        require($this->require);
        return;
    }
}
?>