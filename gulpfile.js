const { watch, src, dest, parallel } = require("gulp");
const less = require("gulp-less");
const minifyCSS = require("gulp-csso");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const base64 = require("gulp-base64-inline");

function css() {
  return src("build/less/*.less")
    .pipe(base64("./node_modules/uikit/src/images/backgrounds/"))
    .pipe(less())
    .pipe(minifyCSS())
    .pipe(rename("wibuzz.theme.min.css"))
    .pipe(dest("app/assets/css"));
}

watch("build/less/**/*.less", css);
exports.css = css;
exports.default = parallel(css);
